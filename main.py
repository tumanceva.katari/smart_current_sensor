import numpy as np
import serial
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler
from scipy.fftpack import fft
import requests

# Параметры Bluetooth
bluetooth_port = 'COM5'
baud_rate = 9600

# Загружаем модели
hair_dryer_model = joblib.load('hair_dryer_model_fft.pkl')
phone_charger_model = joblib.load('phone_charger_model_fft.pkl')

# Загружаем параметры нормализатора
scaler_mean = np.load('scaler_mean.npy')
scaler_scale = np.load('scaler_scale.npy')

# Установка соединения с Arduino через Bluetooth
arduino_serial = serial.Serial(bluetooth_port, baud_rate, timeout=1)

# URL веб-сайта для отправки данных
web_url = 'https://127.0.0.1//5000'

try:
    while True:
        # Считываем данные с Arduino
        current_data = arduino_serial.readline().decode().strip().split(',')

        # Переводим строки в числа
        current_data = [float(val) for val in current_data]

        # Нормализуем данные
        current_data_scaled = (current_data - scaler_mean) / scaler_scale

        # Применяем БПФ
        current_data_fft = np.abs(fft(current_data_scaled))

        # Берем только половину спектра (положительные частоты)
        current_data_fft = current_data_fft[:len(current_data_fft) // 2]

        # Предсказываем метки с использованием обеих моделей
        prediction_hair_dryer = hair_dryer_model.predict([current_data_fft])
        prediction_phone_charger = phone_charger_model.predict([current_data_fft])

        # Выводим результаты
        if prediction_hair_dryer == 1:
            print("Фен обнаружен.")
            device_name = "Hair Dryer"
        elif prediction_phone_charger == 1:
            print("Зарядка телефона обнаружена.")
            device_name = "Phone Charger"
        else:
            print("Не удалось идентифицировать электроприбор.")
            device_name = "Unknown Device"

        # Отправляем данные на веб-сайт
        data_to_send = {'device': device_name, 'current_data': current_data}
        requests.post(web_url, data=data_to_send)

except KeyboardInterrupt:
    print("Программа завершена.")
    arduino_serial.close()

