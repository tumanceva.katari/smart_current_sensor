import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
from scipy.fftpack import fft

# Пути к файлам данных для фена и зарядки телефона
hair_dryer_file_path = 'hair_dryer_data.csv'
phone_charger_file_path = 'phone_charger_data.csv'

# Загружаем данные из файлов
hair_dryer_data = pd.read_csv(hair_dryer_file_path, header=None)
phone_charger_data = pd.read_csv(phone_charger_file_path, header=None)

# Создаем метки для классификации: 0 - фен, 1 - зарядка телефона
hair_dryer_data['label'] = 0
phone_charger_data['label'] = 1

# Объединяем данные для обучения
all_data = pd.concat([hair_dryer_data, phone_charger_data], ignore_index=True)

# Извлекаем спектральные признаки с использованием БПФ
def extract_fft_features(data):
    fft_result = np.abs(fft(data))
    return fft_result[:len(fft_result) // 2]  # Берем только половину спектра (положительные частоты)

# Применяем БПФ к каждому временному ряду
all_data_fft = all_data.iloc[:, :-1].apply(extract_fft_features, axis=1)

# Строим новый DataFrame с извлеченными признаками и метками
all_data_fft['label'] = all_data['label']

# Разделяем данные на признаки (X) и метки (y)
X = all_data_fft.drop('label', axis=1)
y = all_data_fft['label']

# Разбиваем данные на обучающую и тестовую выборки
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Нормализуем данные
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Создаем и обучаем многослойный персептрон (MLP) - нейронную сеть для фена
hair_dryer_model = MLPClassifier(hidden_layer_sizes=(64,), max_iter=1000, random_state=42)
hair_dryer_model.fit(X_train_scaled, y_train)

# Сохраняем модель для фена
joblib.dump(hair_dryer_model, 'hair_dryer_model_fft.pkl')

# Создаем и обучаем многослойный персептрон (MLP) - нейронную сеть для зарядки телефона
phone_charger_model = MLPClassifier(hidden_layer_sizes=(64,), max_iter=1000, random_state=42)
phone_charger_model.fit(X_train_scaled, y_train)

# Сохраняем модель для зарядки телефона
joblib.dump(phone_charger_model, 'phone_charger_model_fft.pkl')

# Сохраняем параметры нормализатора
np.save('scaler_mean.npy', scaler.mean_)
np.save('scaler_scale.npy', scaler.scale_)
