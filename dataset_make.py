import serial
import time
port = 'COM6'
baud_rate = 9600

# Пути к файлам данных для фена и зарядки телефона
hair_dryer_file_path = 'hair_dryer_data.csv'
phone_charger_file_path = 'phone_charger_data.csv'

# Создаем или открываем файлы для записи данных
hair_dryer_file = open(hair_dryer_file_path, 'a')
phone_charger_file = open(phone_charger_file_path, 'a')

# Установка соединения
arduino_serial = serial.Serial(port, baud_rate, timeout=1)

try:
    print("Начало считывания данных с датчика тока...")

    while True:
        # Считываем данные с Arduino
        current_data = arduino_serial.readline().decode().strip()

        # Проверяем, к какому устройству относится текущий сеанс
        device = input("Выберите устройство ('hair_dryer' или 'phone_charger'): ")

        # Записываем данные в соответствующий файл
        if device == 'hair_dryer':
            hair_dryer_file.write(current_data + '\n')
        elif device == 'phone_charger':
            phone_charger_file.write(current_data + '\n')
        else:
            print("Неверное устройство. Повторите ввод.")

except KeyboardInterrupt:
    print("Программа завершена.")
    hair_dryer_file.close()
    phone_charger_file.close()
    arduino_serial.close()
