from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/submit_data', methods=['POST'])
def submit_data():
    device_name = request.form.get('device')
    current_data = request.form.get('current_data')

    return render_template('index.html', device=device_name, current_data=current_data)

if __name__ == '__main__':
    app.run(debug=True)
